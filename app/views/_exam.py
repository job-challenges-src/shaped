from app.models import Exam
from app.api.serializers import ExamSerializer
from app.tasks.task_async_crud import task_create_exam
from core import AsyncModelViewSet

class ExamModelViewSet(AsyncModelViewSet):
    queryset = Exam.objects.all()
    serializer_class = ExamSerializer
    task_def = task_create_exam