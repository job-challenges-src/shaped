from app.models import Patient
from app.api.serializers import PatientSerializer

from app.tasks.task_async_crud import task_create_patient
from core import AsyncModelViewSet


class PatientModelViewSet(AsyncModelViewSet):
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer
    task_def = task_create_patient