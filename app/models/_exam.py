from django.db import models
from django.utils.translation import gettext_lazy as _

    
class Exam(models.Model):
    professional_name = models.CharField(_('Nome do Profissional'), max_length=50)
    exam_date = models.DateField(_('Data do Exame'))
    weight = models.FloatField(_('Peso'))
    height = models.FloatField(_('Altura'))
    patient = models.ForeignKey("app.Patient", verbose_name=_("Paciente"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Exame'
        verbose_name_plural = 'Exames'