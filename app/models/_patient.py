from django.db import models
from django.utils.translation import gettext_lazy as _

    
class Patient(models.Model):
    name = models.CharField(_("Nome"), max_length=150)
    age = models.PositiveSmallIntegerField(_("Idade"))
    address = models.TextField(_('Endereço'))

    class Meta:
        verbose_name = 'Paciente'
        verbose_name_plural = 'Pacientes'