from celery import shared_task
from app.models import Patient, Exam

@shared_task
def task_create_patient(data: dict):
    try:
        print('\n\n\nCriando paciente\n')
        Patient.objects.create(**data)
        print('Paciente criado com sucesso\n\n\n')
    except Exception as e:
        print(e)


@shared_task
def task_create_exam(data: dict):
    try:
        p = Patient.objects.get(pk=data.get('patient'))
        patient_pk = data.get('patient')
        data['patient'] = Patient.objects.filter(pk=patient_pk).first()
        print('\n\n\nCriando Exame\n', p)
        Exam.objects.create(**data)
        print('Exame criado com sucesso\n\n\n')
    except Exception as e:
        print(e)