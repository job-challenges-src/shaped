from django.urls import path, include

from rest_framework.routers import SimpleRouter

from app.views import ExamModelViewSet, PatientModelViewSet

router = SimpleRouter()
router.register(r'exames', ExamModelViewSet)
router.register(r'pacientes', PatientModelViewSet)


urlpatterns = [
  path('/', include(router.urls)),
]
