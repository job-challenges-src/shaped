from rest_framework.serializers import ModelSerializer

from app.models import Exam


class ExamSerializer(ModelSerializer):
    class Meta:
        model = Exam
        fields = '__all__'
