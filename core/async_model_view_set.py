from typing import Callable, Any
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_500_INTERNAL_SERVER_ERROR


class AsyncModelViewSet(ModelViewSet):

    task_def: Callable = None

    def __init__(self, **kwargs: Any) -> None:
        if not self.task_def:
            raise NotImplementedError('task_def does not None!')

        super().__init__(**kwargs)
   
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            self.task_def.delay(serializer.data)
            return Response(status=HTTP_201_CREATED)
        except Exception:
            return Response(status=HTTP_500_INTERNAL_SERVER_ERROR)
